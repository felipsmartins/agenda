package agenda;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ContactGroup {
    private String name;

    private List<Contact> contacts;

    ContactGroup(String name) {
        this.name = name;
        this.contacts = new ArrayList<>();
    }

    public String toString() {
        return this.name;
    }

    boolean addMember(Contact contact) {
        return contacts.add(contact);
    }

    boolean addMember(List<Contact> contacts) {
        return this.contacts.addAll(contacts);
    }

    Iterator<Contact> getAllContacts() {
        return this.contacts.iterator();
    }

    void listContacts() {
        for (Contact contact : contacts) {
            System.out.println(String.format("%s\ngroup: %s\n", contact, this));
        }
    }

    /**
     * Busca contato por email.
     * Tão logo encontra o contato ele retorna,
     * não importanto se o contato está em outros grupos.
     *
     * @param email O e-mail do contato. Isso age como uma PK.
     * @return Retorna o objeto representando o contato ou null caso não encontre.
     */
    Contact searchContact(String email) {
        for (Contact contact : contacts) {
            if (contact.email.equals(email)) {
                return contact;
            }
        }

        return null;
    }
}

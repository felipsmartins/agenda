package agenda;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Application "boostrap".
 */
class Application {
    private final static int MENU_CHOICE_EXIT = 0;
    private final static int MENU_CHOICE_LIST_CONTACTS = 1;
    private final static int MENU_CHOICE_CREATE_GROUP = 2;
    private final static int MENU_CHOICE_CREATE_CONTACT = 3;

    private void print(String _string) {
        System.out.println(_string);
    }

    /**
     * Solicita ao usuário que entre com uma opção do menu.
     */
    private int enterMenuChoice() {
        print("");
        print("===== AGENDA: MENU PRINCIPAL ======");
        print("[1] Listar contatos da agenda");
        print("[2] Adicionar grupo");
        print("[3] Adicionar contato");
        print("[0] SAIR/Encerrar");

        System.out.print("\n> ");
        Scanner scanner = new Scanner(System.in);

        try {
            return scanner.nextInt();
        } catch (java.util.InputMismatchException e) {
            return 0; // encerra por padrão.
        }
    }

    void run() {
        Agenda agenda = new Agenda();

//        ArrayList<Contact> contacts = new ArrayList<>();
//        contacts.add(new Contact("Felipe Martins", "fmartins@mail.com", "8599998888"));
//        contacts.add(new Contact("Maria Pontes", "p.maria@mail.com", "8533998800"));
//
//        ContactGroup favGroup = new ContactGroup("Favoritos");
//        ContactGroup workGroup = new ContactGroup("Trabalho");
//        favGroup.addMember(contacts);
//        workGroup.addMember(new Contact("Paulo Goes", "p.goes@mail.com", "8578998811"));
//
//        agenda.add(favGroup);
//        agenda.add(workGroup);

        // MENU
        int menuChoice;

        while ((menuChoice = enterMenuChoice()) != MENU_CHOICE_EXIT) {
            switch (menuChoice) {
                case MENU_CHOICE_LIST_CONTACTS:
                    agenda.listContacts();
                    break;
                case MENU_CHOICE_CREATE_GROUP:
                    agenda.createGroup();
                    break;
                case MENU_CHOICE_CREATE_CONTACT:
                    agenda.createContact();
                    break;
                default:
                    print("Essa opção não existe!");
                    break;
            }
        }

        print("Encerrando...");
    }
}

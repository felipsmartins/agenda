package agenda;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Agenda de grupos de contatos.
 * Isso não mantém diretamente contatos senão por intemédio de grupos.
 */
class Agenda extends ArrayList<ContactGroup> {

    /**
     * @param email
     * @return
     */
    ContactGroup getGroupByContact(String email) {
        for (ContactGroup group : this) {
            if (group.searchContact(email) instanceof Contact) {
                return group;
            }
        }

        return null;
    }

    void listContacts() {
        for (ContactGroup group : this) {
            group.listContacts();
        }
    }

    void createGroup() {
        System.out.println("[ Novo cadastro ]");
        System.out.print("Entre com o nome do grupo: ");

        Scanner scanner = new Scanner(System.in);
        String name = scanner.next();

        this.add(new ContactGroup(name));

        System.out.println(String.format("Grupo '%s' foi criado, retornando ao menu principal...", name));
    }

    void createContact() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("[ Novo cadastro ]");
        System.out.print("Entre com o nome: ");
        String name = scanner.next();

        System.out.print("Entre com o e-mail: ");
        String email = scanner.next();

        System.out.print("Entre com o telefone: ");
        String phone = scanner.next();

        System.out.println("Entre com um dos grupos abaixo: ");

        for (ContactGroup group: this) {
            int index = this.indexOf(group);
            System.out.println(String.format("[%d] %s", index, group));
        }

        System.out.print("grupo: ");
        int groupChoice = scanner.nextInt();

        Contact contact = new Contact(name, email, phone);
        this.get(groupChoice).addMember(contact);

        System.out.println(String.format("Novo contato de '%s' foi criado, adicionar outro?", contact.name));
        System.out.print("s/n: ");

        String keepAdding = scanner.next();

        if (keepAdding.equals("s")) {
            createContact();
        }
    }
}

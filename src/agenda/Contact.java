package agenda;

public class Contact {

    /**
     * É comum os atributos abaixo serem privados por convenção/proteção,
     * mas nesse caso não há efeito colateral direto por causa disso.
     */

    public String name;
    public String email;
    public String phoneNumber;

    Contact(String name, String email, String phoneNumber) {
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public String toString() {
        return String.format("name: %s\ne-mail: %s\nphone: %s", name, email, phoneNumber);
    }
}

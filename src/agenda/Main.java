package agenda;

import java.util.ArrayList;

public class Main {
    private static void tests() {
        Agenda agenda = new Agenda();

        ArrayList<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact("Felipe Martins", "fmartins@mail.com", "8599998888"));
        contacts.add(new Contact("Maria Pontes", "p.maria@mail.com", "8533998800"));

        ContactGroup favGroup = new ContactGroup("Favoritos");
        ContactGroup workGroup = new ContactGroup("Trabalho");
        favGroup.addMember(contacts);
        workGroup.addMember(new Contact("Paulo Goes", "p.goes@mail.com", "8578998811"));

        agenda.add(favGroup);
        agenda.add(workGroup);
    }


    public static void main(String[] args) {
        Application app = new Application();
        app.run();
    }
}
